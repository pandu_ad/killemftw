﻿using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;

public class CameraController : MonoBehaviour
{
    private Vector3 offset;         //Private variable to store the offset distance between the player and camera
    public float smoothSpeed;

    // Use this for initialization
    void Start()
    {

    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 0)
        {
            Vector3 newPosition = new Vector3(players.Average(x => x.transform.position.x),
                                    players.Average(x => x.transform.position.y),
                                    players.Average(x => x.transform.position.z));
            // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * smoothSpeed);
            transform.position = smoothedPosition;
        }
    }
}