﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;
using UnityEngine.Networking;

public class BoxObstacle : NetworkBehaviour {

    float slowTime;
    bool isTriggered;

    public AudioClip DestroyedFX;

    // Use this for initialization
    void Start () {
        slowTime = 1;
        Invoke("ActivateBox", 0.3f);
    }

    // Run only on server
    private void ActivateBox()
    {
        GetComponent<BoxCollider2D>().enabled = true;
    }

    // Update is called once per frame
    [ServerCallback]
    void Update () {
        if (isTriggered)
        {
            slowTime -= Time.deltaTime;
            if (slowTime < 0)
            {
                GameObject.Destroy(gameObject);
            }
        }
	}

    [ServerCallback]
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" && GetComponent<BoxCollider2D>().isTrigger == false)
        {
            GetComponent<AudioSource>().clip = DestroyedFX;
            GetComponent<AudioSource>().Play();

            Destroy();

            collision.gameObject.GetComponent<PlayerController>().Fall();
            collision.gameObject.GetComponent<PlayerController>().Recover(slowTime);

            RpcCollided(collision.gameObject);
        }
        else if(collision.transform.tag == "Weapon")
        {
            Destroy();
            RpcDestroy();
        }
    }

    [ClientRpc]
    void RpcCollided(GameObject plyr)
    {
        if (!isServer)
        {
            GetComponent<AudioSource>().clip = DestroyedFX;
            GetComponent<AudioSource>().Play();

            Destroy();

            plyr.GetComponent<PlayerController>().Fall();
            plyr.GetComponent<PlayerController>().Recover(slowTime);
        }
    }

    [ClientRpc]
    void RpcDestroy()
    {
        if (!isServer)
        {
            Destroy();
        }
    }

    void Destroy()
    {
        GetComponent<BoxCollider2D>().isTrigger = false;
        isTriggered = true;
    }

}
