﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ObstacleManager : NetworkBehaviour
{
    public GameObject Box;
    public GameObject Ball;
    public GameObject[] Orbs;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Run only on server
    public void ReviveOrb(GameObject orb)
    {
        StartCoroutine(Respawn(orb, 5f));
    }

    // Run only on server
    private IEnumerator Respawn(GameObject orb, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        AssignWeapon(orb);
    }

    // Run only on server
    public void AssignWeapon(GameObject orb)
    {
        int wpnIndex = Random.Range(0,2); // Change when other weapons ready
        RpcAssignWeapon(orb, wpnIndex);
    }

    [ClientRpc]
    public void RpcAssignWeapon(GameObject orb, int wpnIndex)
    {
        orb.SetActive(true);
        switch (wpnIndex)
        {
            case 0:
                orb.GetComponent<Orbs>().Weapon = Box;
                break;
            case 1:
                orb.GetComponent<Orbs>().Weapon = Ball;
                break;
        }
    }
}
