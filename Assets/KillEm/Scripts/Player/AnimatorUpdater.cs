﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AnimatorUpdater : NetworkBehaviour {
    public RuntimeAnimatorController NinjaController;
    public RuntimeAnimatorController DogController;

    [SyncVar]
    int animatorIdx = -1;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isLocalPlayer && animatorIdx < 0)
        {
            CmdInformAnimation(PlayerPrefs.GetInt("CharacterSelected"));
        }

        if (animatorIdx >= 0 && GetComponent<Animator>().runtimeAnimatorController == null)
        {
            switch (animatorIdx)
            {
                case 0:
                    GetComponent<Animator>().runtimeAnimatorController = NinjaController;
                    break;
                case 1:
                    GetComponent<Animator>().runtimeAnimatorController = DogController;
                    break;
            }
        }
    }

    [Command]
    void CmdInformAnimation(int idx)
    {
        animatorIdx = idx;
    }

    public int GetCharIdx()
    {
        return animatorIdx;
    }
}
