﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets._2D;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : NetworkBehaviour
{
    [SyncVar]
    public bool isAlive;
    [SyncVar]
    public int Index;
    [SyncVar]
    public bool WeaponSlotAvaliable;
    [SyncVar]
    private bool isFacingRight = true;

    public GameObject Managers;
    public GameObject ScoreObj;
    public GameObject Weapon;

    // Use this for initialization
    public override void OnStartLocalPlayer()
    {
        transform.Find("Diamond").GetComponent<MeshRenderer>().enabled = true;
        transform.Find("Light").GetComponent<Light>().enabled = true;
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            transform.position = new Vector3(3, 0, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Determine Direction
        if (isLocalPlayer)
        {
            float move = CrossPlatformInputManager.GetAxis("Horizontal");
            if (move > 0 && !isFacingRight)
            {
                // ... flip the player.
                CmdInformDirection(true);
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && isFacingRight)
            {
                // ... flip the player.
                CmdInformDirection(false);
            }
        }

        if (Managers == null)
        {
            Managers = GameObject.Find("Managers");
        }
        else if (isServer && Index == -1)
        {
            Managers.GetComponent<GameStateManager>().PlayerCount++;
            Index = Managers.GetComponent<GameStateManager>().PlayerCount;
        }
        else if (ScoreObj == null)
        {
            Managers = GameObject.Find("Managers");
            if(GameObject.FindGameObjectsWithTag("PlayerIcon").Length > 0)
                ScoreObj = GameObject.FindGameObjectsWithTag("PlayerIcon").First(x => x.name == "Player " + Index);
        }
        else if (Index != -1)
        {
            ScoreObj.GetComponent<Image>().enabled = true;

            UpdateIcon(GetComponent<AnimatorUpdater>().GetCharIdx());
        }

        // Update weapon Icon
        if (ScoreObj != null)
        {
            GameObject weapon = ScoreObj.transform.parent.Find("Weapon").gameObject;
            if (Weapon == null)
            {
                weapon.GetComponent<Image>().enabled = false;
            }
            else
            {
                weapon.GetComponent<Image>().enabled = true;
                switch (Weapon.name)
                {
                    case "Ball":
                        weapon.GetComponent<Image>().sprite = Managers.GetComponent<IconManager>().BallImg;
                        break;
                    case "Box":
                        weapon.GetComponent<Image>().sprite = Managers.GetComponent<IconManager>().BoxImg;
                        break;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            bool fire = Input.GetKey(KeyCode.E);
            if (fire || GetComponent<Platformer2DUserControl>().m_Fire)
            {
                CmdFire();
            }
        }
    }

    [ServerCallback]
    private void OnBecameInvisible()
    {
        if (Managers != null && Managers.GetComponent<RaceManager>().IsGameStarted)
        {
            WaypointListener waypointListener = GetComponent<WaypointListener>();
            RaceManager raceManager = Managers.GetComponent<RaceManager>();
            if (raceManager.IsGameStarted)
            {
                if (waypointListener.LastWaypoint != null)
                {
                    float sign = Mathf.Sign(transform.position.x - waypointListener.LastWaypoint.transform.position.x);
                    int direction = waypointListener.LastWaypoint.GetComponent<WaypointTrigger>().direction;
                    waypointListener.DistancePoint = sign * direction * Vector3.Distance(transform.position, waypointListener.LastWaypoint.transform.position);
                }

                raceManager.EndGame();

                GameStateManager gameStateManager = Managers.GetComponent<GameStateManager>();
                gameStateManager.PlayerDead();
            }
        }
    }

    [Command]
    void CmdInformDirection(bool isfacingright)
    {
        isFacingRight = isfacingright;
    }

    [Command]
    void CmdFire()
    {
        if (Weapon != null)
        {
            GameObject wpn = null;
            if (Weapon.name == "Ball")
            {
                float dir = isFacingRight ? 1 : -1;
                wpn = Instantiate(Weapon, new Vector3(transform.position.x + dir * 3, transform.position.y), transform.rotation);
                wpn.GetComponent<Ball>().Direction = dir;
            }
            else {
                wpn = Instantiate(Weapon, transform.position, transform.rotation);
            }
            NetworkServer.Spawn(wpn);
            Invoke("ActivateWeaponSlot", 3f); // Reactivate after N secs

            Weapon = null;
            RpcFireWeapon(transform.position, transform.rotation);
        }
    }

    [ClientRpc]
    void RpcFireWeapon(Vector3 position, Quaternion rotation)
    {
        if(!isServer)
            Weapon = null;
    }

    void ActivateWeaponSlot()
    {
        WeaponSlotAvaliable = true;
    }

    void UpdateIcon(int idx)
    {
        switch (idx)
        {
            case 0:
                ScoreObj.GetComponent<Image>().sprite = Managers.GetComponent<IconManager>().NinjaImg;
                break;
            case 1:
                ScoreObj.GetComponent<Image>().sprite = Managers.GetComponent<IconManager>().DogImg;
                break;
        }
    }

    public void Fall()
    {
        GetComponent<PlayerSoundController>().PlayFall();

        GetComponent<Animator>().SetBool("Fall", true);
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        GetComponent<Rigidbody2D>().angularVelocity = 0;
        GetComponent<Platformer2DUserControl>().IsMovable = false;
    }

    public void Recover(float time)
    {
        Invoke("Rcvr", time);
    }

    public void Rcvr()
    {
        GetComponent<Animator>().SetBool("Fall", false);
        GetComponent<Platformer2DUserControl>().IsMovable = true;
    }
}
