﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSoundController : NetworkBehaviour
{
    public AudioClip FallFX;
    public AudioClip JumpFX;

    public void PlayJump()
    {
        GetComponent<AudioSource>().clip = JumpFX;
        GetComponent<AudioSource>().Play();
    }

    public void PlayFall()
    {
        GetComponent<AudioSource>().clip = FallFX;
        GetComponent<AudioSource>().Play();
    }
}
