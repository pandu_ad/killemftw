﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LobbyUpdater : NetworkBehaviour {

	// Use this for initialization
	void Start () {
        // NetworkLobbyManager manager = GetComponent<NetworkLobbyManager>();
        // manager.playScene = "Lintasan " + PlayerPrefs.GetInt("TrackSelected") + " Integrated";
        NetworkLobbyManager.singleton.ServerChangeScene("Lintasan " + PlayerPrefs.GetInt("TrackSelected") + " Integrated");
	}
	
	// Update is called once per frame
    void Update () {
        //manager.playScene = "Lintasan " + PlayerPrefs.GetInt("TrackSelected") + " Integrated";
	}
}
