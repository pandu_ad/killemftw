﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class MainMenuButtons : MonoBehaviour
{

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }	

    public void SelectScene(int index)
    {
        PlayerPrefs.SetInt("TrackSelected", index);
    }
    
    public void ExitLobbyAndRedirect(string sceneName)
    {
        Destroy(NetworkLobbyManager.singleton.gameObject);
        NetworkManager.Shutdown();
        SceneManager.LoadScene(sceneName);
    }
}
