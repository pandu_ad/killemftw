﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
    private GameObject[] characterList;
    private int index;

    private void Start()
    {
        characterList = new GameObject[transform.childCount];

        index = PlayerPrefs.GetInt("CharacterSelected");

        //isi array dengan jumlah character 
        for (int i = 0; i < transform.childCount; i++)
            characterList[i] = transform.GetChild(i).gameObject;

        //menghilangkan tampilan character (render)
        foreach (GameObject go in characterList)
            go.SetActive(false);

        //tampilkan character yang dipilih
        if (characterList[index])
            characterList[index].SetActive(true);
    }

    public void ToggleLeft()
    {
        //sembunyikan karakter saat ini
        characterList[index].SetActive(false);

        //karakter sebelumnya
        index--; //index = index - 1;

        if (index < 0)
            index = characterList.Length - 1;

        //munculkan karakter sebelumnya
        characterList[index].SetActive(true);
    }

    public void ToggleRight()
    {
        //sembunyikan karakter saat ini
        characterList[index].SetActive(false);

        //karakter setelahnya
        index++; //index = index + 1;

        if (index == characterList.Length)
            index = 0;

        //munculkan karakter setelahnya
        characterList[index].SetActive(true);
    }

    public void ConfirmButton()
    {
        //Konfirmasi karakter dan track yang dipilih oleh player
        PlayerPrefs.SetInt("CharacterSelected", index);

        //ke scene selanjutnya
        SceneManager.LoadScene("Lobby");
    }

}
