﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Killzone : NetworkBehaviour
{
    public GameObject Managers;

    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Managers.GetComponent<RaceManager>().EndGame();
            Managers.GetComponent<GameStateManager>().PlayerDead(other.gameObject);
        }
    }
}
