﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityStandardAssets._2D;
using System.Linq;
using UnityEngine.SceneManagement;
using Prototype.NetworkLobby;

public class RaceManager : NetworkBehaviour
{
    public float CountDown;
    public GameObject CounterUI;
    public GameObject ResultUI;

    [SyncVar]
    private float count;
    [SyncVar]
    public bool IsGameStarted;
    [SyncVar]
    public bool IsFinished = false;

    private void Awake()
    {
        count = CountDown;
    }

    public void Update()
    {
        if (isServer)
        {
            List<ScoreHandler> scores = GameObject.FindGameObjectsWithTag("Score").Select(x => x.GetComponent<ScoreHandler>()).ToList();
            ScoreHandler winner = scores.FirstOrDefault(x => x.Score == 3);
            if (winner != null)
            {
                RpcFinishGame(winner.gameObject);
                IsFinished = true;
            }
        }

        if (!IsFinished)
        {
            if (!IsGameStarted)
            {
                CounterUI.GetComponent<Text>().text = Mathf.Ceil(count).ToString();

                if (isServer)
                {
                    count -= Time.deltaTime;
                    if (count < 0)
                    {
                        IsGameStarted = true;
                        RpcActivateMovement();

                        count = CountDown;
                    }
                }
            }
            else if (CounterUI.GetComponent<Text>().text != string.Empty)
            {
                CounterUI.GetComponent<Text>().text = string.Empty;
            }
        }
    }

    [ClientRpc]
    public void RpcFinishGame(GameObject winner)
    {
        ResultUI.transform.Find("Winner").GetComponent<Text>().text = winner.transform.parent.name;
        ResultUI.transform.Find("Image").GetComponent<Image>().sprite = winner.transform.parent.GetComponent<Image>().sprite;
        ResultUI.SetActive(true);

        Invoke("RedirectBack", 5f);
    }

    public void RedirectBack()
    {
        NetworkLobbyManager.singleton.GetComponent<LobbyManager>().GoBackButton();
    }

    public void EndGame()
    {
        IsGameStarted = false;
    }

    [ClientRpc]
    public void RpcActivateMovement()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            player.GetComponent<Platformer2DUserControl>().IsMovable = true;
        }
    }
}
