﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;
using UnityStandardAssets._2D;

public class GameStateManager : NetworkBehaviour
{
    [SyncVar]
    public int PlayerCount;

    public GameObject CameraRig;
    public GameObject Waypoints;

    private Transform spawnPoint;

    // Update is called once per frame
    [ServerCallback]
	void Update ()
    {
        if (spawnPoint != null)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player").Where(x => x.GetComponent<PlayerController>().isAlive).ToArray();
            if (players.Length == 0)
            {
                RpcRespawn(spawnPoint.position, spawnPoint.rotation);
            }
        }
    }
    
    // Only called on Server
    public void PlayerDead(GameObject deadPlayer = null)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player").Where(x => x.GetComponent<PlayerController>().isAlive).ToArray();
        if (players.Length <= 2)
        {
            if(deadPlayer == null)
            {
                deadPlayer = findDeadPlayer(players);
            }
            spawnPoint = deadPlayer.GetComponent<WaypointListener>().LastWaypoint.transform;

            GameObject winningPlayer = players.First(x => x != deadPlayer);
            SrvUpdateScore(winningPlayer);

            foreach (GameObject player in players)
            {
                RpcRemovePlayer(player);
            }
            CameraRig.transform.position = spawnPoint.position;
        }

        if (players.Length > 2)
        {
            deadPlayer = findDeadPlayer(players);
            RpcRemovePlayer(deadPlayer);
        }
    }
    
    public void SrvUpdateScore(GameObject player)
    {
        int index = player.GetComponent<PlayerController>().Index;
        ScoreHandler score = GameObject.Find("Player " + index).transform.GetChild(0).gameObject.GetComponent<ScoreHandler>();
        score.Score++;
    }

    [ClientRpc]
    public void RpcRemovePlayer(GameObject player)
    {
        player.GetComponent<SpriteRenderer>().enabled = false;
        player.GetComponent<Platformer2DUserControl>().IsMovable = false;
        player.GetComponent<PlayerController>().isAlive = false;
    }

    GameObject findDeadPlayer(GameObject[] players)
    {
        int waypointCount = Waypoints.GetComponent<WayPointsManager>().WayPointNumber;
        float minWaypoint = players.Select(x => x.GetComponent<WaypointListener>().Lap * waypointCount + x.GetComponent<WaypointListener>().LastWaypoint.GetComponent<WaypointTrigger>().Number).Min();
        GameObject[] filteredPlayer = players.Where(x => x.GetComponent<WaypointListener>().Lap * waypointCount +
                                                    x.GetComponent<WaypointListener>().LastWaypoint.GetComponent<WaypointTrigger>().Number
                                                    == minWaypoint).ToArray();
        if (filteredPlayer.Length > 1)
        {
            float minDistance = filteredPlayer.Select(x => x.GetComponent<WaypointListener>().DistancePoint).Min();
            return filteredPlayer.FirstOrDefault(x => x.GetComponent<WaypointListener>().DistancePoint == minDistance);
        }
        else
        {
            return filteredPlayer[0];
        }
    }

    [ClientRpc]
    public void RpcRespawn(Vector3 position, Quaternion rotation)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            player.transform.position = position;
            player.transform.rotation = rotation;
            player.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            player.GetComponent<SpriteRenderer>().enabled = true;
            player.GetComponent<PlayerController>().isAlive = true;
        }
    }
}
