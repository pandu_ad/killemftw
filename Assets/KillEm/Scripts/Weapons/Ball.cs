﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets._2D;
using UnityStandardAssets.CrossPlatformInput;

public class Ball : NetworkBehaviour
{
    public float Speed = 10f;
    public AudioClip DestroyedFX;
    public float Direction;

    // Update is called once per frame
    [ServerCallback]
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(Direction * Speed, GetComponent<Rigidbody2D>().velocity.y);
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<AudioSource>().Play();

        if (isServer)
        {
            if (collision.transform.tag != "Weapon" && collision.transform.tag != "Player" && collision.transform.tag != "Platform")
            {
                GetComponent<AudioSource>().clip = DestroyedFX;
                GetComponent<AudioSource>().Play();
                Invoke("Destroy", 1f);

                RpcDestroy();
            }
            else if (collision.transform.tag == "Player")
            {
                collision.gameObject.GetComponent<PlayerController>().Fall();
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1400f));
                collision.gameObject.GetComponent<PlayerController>().Recover(1f);

                RpcCollided(collision.gameObject);
            }
        }
    }

    [ClientRpc]
    void RpcDestroy()
    {
        if (!isServer)
        {
            GetComponent<AudioSource>().clip = DestroyedFX;
            GetComponent<AudioSource>().Play();
        }
    }

    void Destroy()
    {
        Destroy(gameObject);
    }

    [ClientRpc]
    void RpcCollided(GameObject plyr)
    {
        if (!isServer)
        {
            plyr.GetComponent<PlayerController>().Fall();
            plyr.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1400f));
            plyr.GetComponent<PlayerController>().Recover(1f);
        }
    }
}
