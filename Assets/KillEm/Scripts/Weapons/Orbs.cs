﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Orbs : NetworkBehaviour {
    public int Index;
    public GameObject Weapon;
    public GameObject Manager;

    // Use this for initialization
    [ServerCallback]
    void Start () {
        
    }

    // Update is called once per frame
    [ServerCallback]
	void Update () {
        if(Weapon == null)
        {
            Manager.GetComponent<ObstacleManager>().AssignWeapon(gameObject);
        }
    }

    // To make sure same player get same weapon
    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && collision.gameObject.GetComponent<PlayerController>().WeaponSlotAvaliable)
        {
            Manager.GetComponent<ObstacleManager>().ReviveOrb(gameObject);
            RpcTriggerEnter(collision.gameObject);
        }
    }

    [ClientRpc]
    void RpcTriggerEnter(GameObject player)
    {
        player.GetComponent<PlayerController>().WeaponSlotAvaliable = false;
        player.GetComponent<PlayerController>().Weapon = Weapon;
        gameObject.SetActive(false);
    }

}
