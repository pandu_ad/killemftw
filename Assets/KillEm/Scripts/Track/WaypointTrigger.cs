﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WaypointTrigger : NetworkBehaviour {
    public int Number;
    public int direction = 1;
    public bool IsMidWaypoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            if (IsMidWaypoint)
            {
                WaypointListener waypointListerner = other.GetComponent<WaypointListener>();
                waypointListerner.IsHalfWay = true;
            }
            if (Number == 0)
            {
                WaypointListener waypointListerner = other.GetComponent<WaypointListener>();
                if (waypointListerner.IsHalfWay)
                {
                    waypointListerner.Lap++;
                    waypointListerner.IsHalfWay = false;
                }
            }

            other.GetComponent<WaypointListener>().LastWaypoint = gameObject;
            //RpcTriggered(other.gameObject);
        }
    }

    [ClientRpc]
    void RpcTriggered(GameObject plyr)
    {
        if (IsMidWaypoint)
        {
            WaypointListener waypointListerner = plyr.GetComponent<WaypointListener>();
            waypointListerner.IsHalfWay = true;
        }
        if (Number == 0)
        {
            WaypointListener waypointListerner = plyr.GetComponent<WaypointListener>();
            if (waypointListerner.IsHalfWay)
            {
                waypointListerner.Lap++;
                waypointListerner.IsHalfWay = false;
            }
        }

        plyr.GetComponent<WaypointListener>().LastWaypoint = gameObject;
    }
}
