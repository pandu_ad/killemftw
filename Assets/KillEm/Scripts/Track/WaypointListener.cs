﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

class WaypointListener : NetworkBehaviour
{
    [SyncVar]
    public float DistancePoint;

    [SyncVar]
    public GameObject LastWaypoint;

    [SyncVar]
    public bool IsHalfWay;

    [SyncVar]
    public int Lap;

    [ServerCallback]
    void Awake()
    {
        //DistancePoint = 0;
        //int waypointCount = GameObject.Find("WayPoints").GetComponent<WayPointsManager>().WayPointNumber;
        //LastWaypoint = GameObject.Find("Waypoint " + waypointCount);
    }

    // Update is called once per frame
    [ServerCallback]
    private void Update()
    {
        if(LastWaypoint == null)
        {
            GameObject waypoints = GameObject.Find("WayPoints");

            if (waypoints != null)
            {
                DistancePoint = 0;
                int waypointCount = GameObject.Find("WayPoints").GetComponent<WayPointsManager>().WayPointNumber;
                LastWaypoint = GameObject.Find("Waypoint " + waypointCount);
            }
        }
        else
        {
            float sign = Mathf.Sign(transform.position.x - LastWaypoint.transform.position.x);
            int direction = LastWaypoint.GetComponent<WaypointTrigger>().direction;
            DistancePoint = sign * direction * Vector3.Distance(transform.position, LastWaypoint.transform.position);
        }
    }

    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Waypoint")
        {
            LastWaypoint = other.gameObject;
        }
    }
}
