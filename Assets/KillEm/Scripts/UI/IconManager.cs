﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconManager : MonoBehaviour {
    [Header("Animations")]
    public Sprite DogImg;
    public Sprite CatImg;
    public Sprite NinjaImg;
    public Sprite ZombieImg;

    [Header("Weapons")]
    public Sprite BoxImg;
    public Sprite BallImg;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
