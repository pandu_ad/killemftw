using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D
{
    public class Restarter : NetworkBehaviour
    {
        [ServerCallback]
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                new NetworkManager().ServerChangeScene(SceneManager.GetSceneAt(0).name);
            }
        }
    }
}
