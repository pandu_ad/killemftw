﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

namespace Assets.Standard_Assets._2D.Scripts
{
    class SyncPosition : NetworkBehaviour
    {
        [SyncVar]
        Vector3 SyncPos;

        [SerializeField]
        float lerpRate = 15;

        private void FixedUpdate()
        {
            TransmitPosition();
            LerpPosition();
        }

        void LerpPosition()
        {
            if (!isLocalPlayer)
            {
                transform.position = Vector3.Lerp(transform.position, SyncPos, Time.deltaTime*lerpRate);
            }
        }

        [Command]
        void CmdProvidePositionToServer(Vector3 pos)
        {
            SyncPos = pos;
        }

        void TransmitPosition()
        {
            if (isLocalPlayer)
            {
                CmdProvidePositionToServer(transform.position);
            }
        }
    }
}
