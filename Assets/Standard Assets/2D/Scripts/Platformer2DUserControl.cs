using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : NetworkBehaviour
    {
        private PlatformerCharacter2D m_Character;
        private bool m_Jump;
        public bool m_Fire;
        public bool ContinousJump;

        [SyncVar]
        public bool IsMovable;

        public AudioClip FallFX;
        public AudioClip JumpFX;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                if (ContinousJump)
                {
                    // Read the jump input in Update so button presses aren't missed.
                    m_Jump = CrossPlatformInputManager.GetButton("Jump");
                }
                else
                {
                    // Read the jump input in Update so button presses aren't missed.
                    m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
                }
            }
            //if (!m_Fire)
            //{
                    // Read the jump input in Update so button presses aren't missed.
                    m_Fire = CrossPlatformInputManager.GetButton("Fire");
            //}
        }

        private void FixedUpdate()
        {
            if (isLocalPlayer && IsMovable)
            {
                // Read the inputs.
                bool crouch = Input.GetKey(KeyCode.LeftControl);
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                // Pass all parameters to the character control script.
                CmdUpdatePlayer(h, crouch, m_Jump);
                m_Jump = false;
            }
        }

        [Command]
        private void CmdUpdatePlayer(float move, bool crouch, bool jump)
        {
           m_Character.RpcMove(move, crouch, jump);
        }

        public void PlayJump()
        {
            GetComponent<AudioSource>().clip = JumpFX;
            GetComponent<AudioSource>().Play();
        }
    }
}
